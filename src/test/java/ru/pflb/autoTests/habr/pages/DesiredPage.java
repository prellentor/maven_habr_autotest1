package ru.pflb.autoTests.habr.pages;

import org.openqa.selenium.WebElement;

public class DesiredPage extends BasePage {
    public void clickMostRead (Integer index)
    {
        String resultXPathTemplate = "//*[@id=\"broadcast_posts_today\"]/ul//li[1]";
        String resultXpath = String.format(resultXPathTemplate, index);
        WebElement desiredLink = driver.findElementByXPath(resultXpath);
        desiredLink.click();
    }
}
