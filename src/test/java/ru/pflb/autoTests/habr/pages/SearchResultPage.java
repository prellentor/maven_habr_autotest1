package ru.pflb.autoTests.habr.pages;

import org.openqa.selenium.WebElement;

public class SearchResultPage extends BasePage{

    public void clickSortByRait()
    {
        String resultXpath = "//a[text()=\"по рейтингу\"]";////ul[@class="toggle-menu"]//li[3]//a
        WebElement searchField = driver.findElementByXPath(resultXpath);
        searchField.click();
    }

    public void clickDismisCookie()
    {
        String resultXpath = "//a[@aria-label='notice-dismiss']";
        WebElement searchField = driver.findElementByXPath(resultXpath);
        searchField.click();
    }

    public void clickSearchResultElement (int index)
    {
        String resultXPathTemplate = "//li[@class=\"content-list__item content-list__item_post shortcuts_item\"][\"%d\"]//h2//a";
        String resultXpath = String.format(resultXPathTemplate, index);
        WebElement desiredLink = driver.findElementByXPath(resultXpath);
        desiredLink.click();
    }
}
