package ru.pflb.autoTests.habr.pages;

import org.openqa.selenium.WebElement;

public class MainPage extends BasePage {

    public void open (String link)
    {   driver.get(link);  }

    public void close ()
    {   driver.ClosePage();   }

    public void quit ()
    {   driver.ClosePage();   }

    public void openSearchField ()
    {
        WebElement searchButton = driver.findElementByXPath("//*[@id=\"search-form-btn\"]");
        searchButton.click();
    }

    public void setSearchFieldAndSubmit (String searchQuery)
    {
        WebElement searchField = driver.findElementByXPath("//input[@id=\"search-form-field\"]");
        searchField.sendKeys(searchQuery);
        searchField.submit();
    }

    public void clickOnRegistraion ()
    {
        WebElement registrationLink = driver.findElementByXPath("//li//a[text()=\"Войти\"]");
        registrationLink.click();
    }

    public String getFooterAdvertLink()
    {
        String link = "";
        WebElement advertLink  = driver.findElementByXPath("//ul[@class=\"footer-menu\"]//a[contains(text(),\"Реклама\")]");
        link = advertLink.getAttribute("href");
        return link;
    }
}
