package ru.pflb.autoTests.habr.pages;

import org.openqa.selenium.*;

import java.util.List;

public class RegistrationPage extends BasePage{
    public boolean checkForCaptcha()
    {
        // Допустимо ли такое сокращение? Или так писать не стоит?
        driver.findElementByXPath("//*[@id=\"email_field\"]")
                .sendKeys("ScorchedPicachu@gmail.com");

        driver.findElementByXPath("//*[@id=\"nickname_field\"]")
                .sendKeys("Scorched_Picachu");

        driver.findElementByXPath("//*[@id=\"password_field\"]")
                .sendKeys("password");

        driver.findElementByXPath("//*[@id=\"password_repeat\"]")
                .sendKeys("password");

        driver.findElementByXPath("//*[@id=\"captcha\"]")
                .sendKeys("password");

        driver.findElementByXPath("//*[@id=\"registration_button\"]")
                .click();

        WebElement captchaNotification = driver.findElementByXPath("//div[contains(text(),\"Неверно указаны символы\")]");
        if (captchaNotification != null)
            return true;

        return false;
    }
}
