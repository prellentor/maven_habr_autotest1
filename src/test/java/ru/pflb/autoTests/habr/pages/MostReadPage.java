package ru.pflb.autoTests.habr.pages;

import org.openqa.selenium.WebElement;

public class MostReadPage extends BasePage {
    public void clickLoginOnTheBottom ()
    {
        String resultXpath = "//li//a[text()=\"Регистрация\"]";
        WebElement loginLink = driver.findElementByXPath(resultXpath);
        loginLink.click();
    }
}
