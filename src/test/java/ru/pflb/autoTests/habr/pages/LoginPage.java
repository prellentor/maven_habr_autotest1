package ru.pflb.autoTests.habr.pages;

import org.openqa.selenium.WebElement;

public class LoginPage extends BasePage {

    public void tryLoginUnderWrongEmail ()
    {
        WebElement emailField = driver.findElementByXPath("//*[@id=\"email_field\"]");
        emailField.sendKeys("Farshtv@mailcom");

        WebElement passwordField = driver.findElementByXPath("//*[@id=\"password_field\"]");
        passwordField.sendKeys("JF9_g-g#GS");
    }

    public boolean checkForWrongEmail ()
    {
        WebElement notification = driver.findElementByXPath("//div[contains(text(),\"Введите корректный e-mail\")]");
        if (notification != null)
            return true;
        return false;
    }

}
