package ru.pflb.autoTests.habr;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.pflb.autoTests.habr.pages.*;

public class TestCase {
    MainPage mainPage;
    SearchResultPage resultPage;
    DesiredPage desiredPage;
    MostReadPage mostReadPage;
    LoginPage loginPage;
    RegistrationPage registrationPage;
    AdvertismentPage advertismentPage;

    @BeforeClass
    void setUpClass ()
    {
        mainPage = new MainPage();
        resultPage = new SearchResultPage();
        desiredPage = new DesiredPage();
        mostReadPage = new MostReadPage();
        loginPage = new LoginPage();
        registrationPage = new RegistrationPage();
        advertismentPage = new AdvertismentPage();
    }

    @AfterClass
    void tearDownClass ()
    {   mainPage.quit();    }

    @BeforeMethod
    void setUpMethod ()
    {   mainPage.open("http://habr.com");   }

    @Test
    void test ()
    {
        // OpenSearchField
        mainPage.openSearchField ();
        // Perform search on this site by "xpath" and fall to search results page.
        mainPage.setSearchFieldAndSubmit("xpath");
        // Sort results by highest rating.
        resultPage.clickSortByRait();
        // Click on fourth result link.
        resultPage.clickSearchResultElement(4);
        // Click on first link in "most read" section at the bottom.
        desiredPage.clickMostRead(1);
        // Click on login link at the bottom
        mostReadPage.clickLoginOnTheBottom();
        // Fill the fields on login page with wrong params
        loginPage.tryLoginUnderWrongEmail();
        // Check for "Enter correct e-mail" field
        boolean b = loginPage.checkForWrongEmail();
        Assert.assertEquals(b, true);
    }

    //Test for wrong captcha symbols notification
    @Test
    void test2 ()
    {
        mainPage.clickOnRegistraion();
        //fill fields with data and check for wrong captcha symbols notification
        boolean b = registrationPage.checkForCaptcha();
        Assert.assertEquals(b, true);
    }

    @Test
    void test3 () {
        boolean b = false;
        String link = mainPage.getFooterAdvertLink();
        if ("link" != "")
        {
            mainPage.open(link);
            b = advertismentPage.checkForString();
        }
        Assert.assertEquals(b, true);
    }
}
