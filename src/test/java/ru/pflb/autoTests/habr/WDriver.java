package ru.pflb.autoTests.habr;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

//обертка над
public class WDriver {
    private static WDriver ourInstance = new WDriver();
    private static WebDriverWait wait;
    private static ChromeDriver driver;
    private static Logger log;
    public static WDriver getInstance() {
        return ourInstance;
    }

    private WDriver() {
        //создаем экземпляр класса Логгера
        log = LogManager.getLogger();
        // добавляем в свойства системы  ???что-то??? что позволяет нам использовать хромдрайвер
        System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");
        // создаем экземпляр класса настройки хрома и вырубаем всплывающие уведомления
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("disable-infobars");
        // инициализируем экземпляр драйвера хрома с настройками
        driver = new ChromeDriver(chromeOptions);
        // разворачиваем окно хрома на весь экран
        driver.manage().window().maximize();
        // Таймер ожидания веб драйвера настроен так, что при исполнении команд поиска
        // через 10 секунд выполнение программы прервётся с ошибкой,
        // с попыткой в каждые 1/4 секунды найти нужный элемент
        wait = new WebDriverWait(driver, 10,250);
    }

    public void ClosePage () {
        driver.close();
    }

    public void CloseDriver () { driver.quit(); }

    public void get(String url) {
        // Запись в историю сообщения о попытке открыть сайт
        log.trace(String.format("Open site by address '%s'.", url));
        // Открытие веб страницы драйвером
        driver.get(url);
    }

    public WebElement findElementByXPath(String Xpath)
    {
        // Запись в историю сообщения о попытке найти обьект на странице
        log.trace(String.format("Searching for element by '%s' path.", Xpath));
        //WebElement element = driver.findElementByXPath(Xpath);
        // создание экземпляра обьекта
        WebElement element = null;
        //попытка найти обьект спроверкой
        try {
            //на наличие, видимость, активность"кликабельность"
            element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Xpath)));
            element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath)));
            element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Xpath)));
        }
        catch (TimeoutException timeoutException) {// время на поиск вышло
            // драйвер делает скриншот и мы создаем из него файл
            File file = ((TakesScreenshot)driver).
                    getScreenshotAs(OutputType.FILE);
            // попытка сохранить файл
            try {
                SimpleDateFormat format =
                        new SimpleDateFormat("dd.MM.HH-mm-ss");
                Date date = new Date();

                FileUtils.copyFile(file, new File(
                        String.format("screenshots/%s-scr.jpg",
                                format.format(date))));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }//только если наш элемент мы перемещаем страницу чтобы был виден элемент
        if (element != null)
        {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        }
       /* Actions scroll = new Actions(driver);
        scroll.moveToElement(element).perform();*/
        return element;
    }
}
